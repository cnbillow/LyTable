# lyTable

> 一个基于CSS3的相响应式表格

## 说明

> 欢迎大家参考学习,也望大家能够对本软件中的不足或错误进行指正批评.

开发者: **刘越(HangxingLiu)**

![我的微博:](http://www.sinaimg.cn/blog/developer/wiki/LOGO_16x16.png)[@航行刘](http://weibo.com/chinavl)

开源许可证: [Apache License Version 2.](http://git.oschina.net/voyageliu/LyTable/raw/master/LICENSE)

## 使用

``` html
<link rel="stylesheet" type="text/css" href="lytable.css"/>
<script src="jquery.min.js" type="text/javascript"></script>
<script src="lytable.js" type="text/javascript"></script>
```

``` html
<table class="table" cellspacing="0">
	<thead>
		<tr>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td></td>
		</tr>
	</tbody>
</table>
```

``` javascript
$('table').lyTable();
```

