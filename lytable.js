$.fn.lyTable = function() {
	headName = [];
	headLength = 0;
	this.find('thead th').each(function(i) {
		headName[headLength++] = $(this).text()
	});
	this.find('tbody tr').each(function(i) {
		$(this).children('td').each(function(j) {
			$(this).attr('data-lytb', headName[j])
		})
	})
};